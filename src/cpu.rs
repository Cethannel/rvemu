use crate::bus::*;
use crate::dram::*;
use crate::exceptions::Exception;
use crate::params::DRAM_BASE;
use crate::params::DRAM_SIZE;
use log::error;

pub struct Cpu {
    pub regs: [u64; 32],
    pub pc: u64,
    pub csrs: [u64; 4096],
    pub bus: Bus,
}

impl Cpu {
    pub fn new(binary: Vec<u8>) -> Self {
        let mut regs = [0; 32];
        regs[2] = DRAM_BASE + DRAM_SIZE;

        Self {
            regs,
            pc: DRAM_BASE,
            csrs: [0; 4096],
            bus: Bus::new(binary),
        }
    }

    /// Load a value from a dram.
    pub fn load(&mut self, addr: u64, size: u64) -> Result<u64, Exception> {
        self.bus.load(addr, size)
    }

    /// Store a value to a dram.
    pub fn store(&mut self, addr: u64, size: u64, value: u64) -> Result<(), Exception> {
        self.bus.store(addr, size, value)
    }

    fn load_csr(&self, addr: usize) -> u64 {
        match addr {
            SIE => self.csrs[MIE] & self.csrs[MIDELEG],
            _ => self.csrs[addr],
        }
    }

    fn store_csr(&mut self, addr: usize, value: u64) {
        match addr {
            SIE => {
                self.csrs[MIE] = (self.csrs[MIE] & !self.csrs[MIDELEG]) | (value & self.csrs[MIDELEG]);
            }
            _ => self.csrs[addr] = value;
        }
    }

    pub fn fetch(&self) -> Result<u64, Exception> {
        self.bus.load(self.pc, 32)
    }

    pub fn dump_registers(&self) {
        let mut output = String::from("");
        let abi = [
            "zero", " ra ", " sp ", " gp ", " tp ", " t0 ", " t1 ", " t2 ", " s0 ", " s1 ", " a0 ",
            " a1 ", " a2 ", " a3 ", " a4 ", " a5 ", " a6 ", " a7 ", " s2 ", " s3 ", " s4 ", " s5 ",
            " s6 ", " s7 ", " s8 ", " s9 ", " s10", " s11", " t3 ", " t4 ", " t5 ", " t6 ",
        ];
        for i in (0..32).step_by(4) {
            output = format!(
                "{}\n{}",
                output,
                format!(
                    "x{:02}({})={:>#18x} x{:02}({})={:>#18x} x{:02}({})={:>#18x} x{:02}({})={:>#18x}",
                    i,
                    abi[i],
                    self.regs[i],
                    i + 1,
                    abi[i + 1],
                    self.regs[i + 1],
                    i + 2,
                    abi[i + 2],
                    self.regs[i + 2],
                    i + 3,
                    abi[i + 3],
                    self.regs[i + 3],
                )
            );
        }
        println!("{}", output);
    }

    pub fn execute(&mut self, inst: u64) -> Result<u64, Exception> {
        let opcode = inst & 0x7f;
        let rd = ((inst >> 7) & 0x1f) as usize;
        let rs1 = ((inst >> 15) & 0x1f) as usize;
        let rs2 = ((inst >> 20) & 0x1f) as usize;
        let csr = inst >> 20;
        let funct3 = (inst >> 12) & 0x7;
        let funct7 = (inst >> 25) & 0x7;

        self.regs[0] = 0;

        match opcode {
            // Load instructions
            0x03 => {
                let imm = ((inst as i32 as i64) >> 20) as u64;
                let addr = self.regs[rs1].wrapping_add(imm);

                match funct3 {
                    // lb
                    0x0 => {
                        let val = self.load(addr, 8)?;
                        self.regs[rd] = val as i8 as i64 as u64;
                    }
                    // lh
                    0x1 => {
                        let val = self.load(addr, 16)?;
                        self.regs[rd] = val as i16 as i64 as u64;
                    }
                    // lw
                    0x2 => {
                        let val = self.load(addr, 32)?;
                        self.regs[rd] = val as i16 as i64 as u64;
                    }
                    // ld
                    0x3 => {
                        let val = self.load(addr, 64)?;
                        self.regs[rd] = val;
                    }
                    // lbu
                    0x4 => {
                        let val = self.load(addr, 8)?;
                        self.regs[rd] = val;
                    }
                    // lhu
                    0x5 => {
                        let val = self.load(addr, 16)?;
                        self.regs[rd] = val;
                    }
                    // lwu
                    0x6 => {
                        let val = self.load(addr, 32)?;
                        self.regs[rd] = val;
                    }
                    _ => {
                        error!(
                            "LOAD: not implemented yet {:#x} func3 {:#x}",
                            opcode, funct3
                        );
                        return Err(Exception::IllegalInstruction(inst));
                    }
                }
            }
            0x13 => {
                let imm = ((inst & 0xfff00000) as i32 as i64 >> 20) as u64;
                let funct6 = funct7 >> 1;

                match funct3 {
                    // addi
                    0x0 => {
                        self.regs[rd] = self.regs[rs1].wrapping_add(imm);
                    }
                    // slli
                    0x1 => {
                        let shamt = (inst >> 20) & 0x3f;
                        self.regs[rd] = self.regs[rs1] << shamt;
                    }
                    // slti
                    0x2 => {
                        self.regs[rd] = if (self.regs[rs1] as i64) < (imm as i64) {
                            1
                        } else {
                            0
                        };
                    }
                    // slti
                    0x3 => {
                        self.regs[rd] = if self.regs[rs1] < imm { 1 } else { 0 };
                    }
                    // xori
                    0x4 => {
                        self.regs[rd] = self.regs[rs1] ^ imm;
                    }
                    0x5 => {
                        let shamt = (inst >> 20) & 0x3f;
                        match funct6 {
                            0x00 => {
                                self.regs[rd] = self.regs[rs1] >> shamt;
                            }
                            0x10 => {
                                self.regs[rd] = ((self.regs[rs1] as i64) >> shamt) as u64;
                            }
                            _ => {
                                return Err(Exception::IllegalInstruction(inst));
                            }
                        }
                    }
                    // ori
                    0x6 => {
                        self.regs[rd] = self.regs[rs1] | imm;
                    }
                    // andi
                    0x7 => {
                        self.regs[rd] = self.regs[rs1] & imm;
                    }
                    _ => {
                        return Err(Exception::IllegalInstruction(inst));
                    }
                }
            }
            // auipc
            0x17 => {
                let imm = (inst & 0xfffff000) as i32 as i64 as u64;
                self.regs[rd] = self.pc.wrapping_add(imm);
            }
            // Store instructions
            0x23 => {
                let imm = (((inst & 0xfe000000) as i32 as i64 >> 20) as u64) | ((inst >> 7) & 0x1f);
                let addr = self.regs[rs1].wrapping_add(imm);
                match funct3 {
                    0x0 => self.store(addr, 8, self.regs[rs2])?,  // sb
                    0x1 => self.store(addr, 16, self.regs[rs2])?, // sh
                    0x2 => self.store(addr, 32, self.regs[rs2])?, // sw
                    0x3 => self.store(addr, 64, self.regs[rs2])?, // sd
                    _ => {
                        error!("Store not implemented or does not exsist: {:#x}", funct3);
                        return Err(Exception::IllegalInstruction(inst));
                    }
                }
            }
            0x33 => {
                match (funct3, funct7) {
                    // add
                    (0x0, 0x00) => {
                        self.regs[rd] = self.regs[rs1].wrapping_add(self.regs[rs2]);
                    }
                    // sub
                    (0x0, 0x20) => {
                        self.regs[rd] = self.regs[rs1].wrapping_sub(self.regs[rs2]);
                    }
                    // sll
                    (0x1, 0x00) => {
                        let shamt = self.regs[rs2] & 0x3f;
                        self.regs[rd] = self.regs[rs1] << shamt;
                    }
                    // slt
                    (0x2, 0x00) => {
                        self.regs[rd] = if (self.regs[rs1] as i64) < (self.regs[rs2] as i64) {
                            1
                        } else {
                            0
                        };
                    }
                    // sltu
                    (0x3, 0x00) => {
                        self.regs[rd] = if self.regs[rs1] < self.regs[rs2] {
                            1
                        } else {
                            0
                        };
                    }
                    // xor
                    (0x4, 0x00) => {
                        self.regs[rd] = self.regs[rs1] ^ self.regs[rs2];
                    }
                    // srl
                    (0x5, 0x00) => {
                        let shamt = self.regs[rs2] & 0x3f;
                        self.regs[rd] = self.regs[rs1] >> shamt;
                    }
                    // sra
                    (0x5, 0x20) => {
                        let shamt = self.regs[rs2] & 0x3f;
                        self.regs[rd] = ((self.regs[rs1] as i64) >> shamt) as u64;
                    }
                    // or
                    (0x6, 0x00) => {
                        self.regs[rd] = self.regs[rs1] | self.regs[rs2];
                    }
                    // and
                    (0x7, 0x00) => {
                        self.regs[rd] = self.regs[rs1] & self.regs[rs2];
                    }
                    _ => return Err(Exception::IllegalInstruction(inst)),
                }
            }
            // lui
            0x37 => {
                self.regs[rd] = (inst & 0xfffff000) as i32 as i64 as u64;
            }
            0x3b => match (funct3, funct7) {
                // addw
                (0x0, 0x0) => {
                    self.regs[rd] = (self.regs[rs1] as u32).wrapping_add(self.regs[rs2] as u32)
                        as i32 as i64 as u64;
                }
                // subw
                (0x0, 0x20) => {
                    self.regs[rd] = (self.regs[rs1] as u32).wrapping_sub(self.regs[rs2] as u32)
                        as i32 as i64 as u64;
                }
                // sllw
                (0x1, 0x00) => {
                    let shamt = self.regs[rs2] & 0x3f;
                    self.regs[rd] = ((self.regs[rs1] as u32) << shamt) as i32 as u64;
                }
                // srlw
                (0x3, 0x00) => {
                    let shamt = self.regs[rs2] & 0x3f;
                    self.regs[rd] = ((self.regs[rs1] as u32) >> shamt) as i32 as u64;
                }
                // sraw
                (0x3, 0x20) => {
                    let shamt = self.regs[rs2] & 0x3f;
                    self.regs[rd] = ((self.regs[rs1] as u32) >> shamt) as i32 as u64;
                }
                _ => return Err(Exception::IllegalInstruction(inst)),
            },
            // branch
            0x63 => {
                let imm = (((inst & 0x80000000) as i32 as i64 >> 19) as u64)
                    | ((inst & 0x80) << 4)
                    | ((inst >> 20) & 0x7e0)
                    | ((inst >> 7) & 0x1e);

                match funct3 {
                    // beq
                    0x0 => {
                        if self.regs[rs1] == self.regs[rs2] {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    // bne
                    0x1 => {
                        if self.regs[rs1] != self.regs[rs2] {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    // blt
                    0x4 => {
                        if (self.regs[rs1] as i64) < (self.regs[rs2] as i64) {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    // bge
                    0x5 => {
                        if (self.regs[rs1] as i64) >= (self.regs[rs2] as i64) {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    // bltu
                    0x6 => {
                        if self.regs[rs1] < self.regs[rs2] {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    // bgeu
                    0x7 => {
                        if self.regs[rs1] >= self.regs[rs2] {
                            self.pc = self.pc.wrapping_add(imm).wrapping_add(4);
                        }
                    }
                    _ => {
                        return Err(Exception::IllegalInstruction(inst));
                    }
                }
            }
            // jalr
            0x67 => {
                let t = self.pc.wrapping_add(4);

                let offset = (inst as i32 as i64) >> 20;
                let target = ((self.regs[rs1] as i64).wrapping_add(offset)) & !1;

                self.pc = (target as u64).wrapping_add(4);

                self.regs[rd] = t;
            }
            // jal
            0x6f => {
                let offset = (((inst & 0x80000000) as i32 as i64) as u64)
                    | (inst & 0xff000)
                    | ((inst >> 9) & 0x800)
                    | ((inst >> 20) & 0x7fe);

                self.regs[rd] = self.pc.wrapping_add(4);
                self.pc = self.pc.wrapping_add(offset).wrapping_add(4);
            }
            0x73 => {
                match (funct3, funct7) {
                    // ecall
                    (0x0, 0x00) => {
                        error!("Ecall not handeled yet");
                        return Err(Exception::IllegalInstruction(inst));
                    }
                    // ebreak
                    (0x0, 0x01) => {
                        error!("Ebreak not handeled yet");
                        return Err(Exception::IllegalInstruction(inst));
                    }
                    (0x1, _) => {
                        self.regs[rd] = csr;
                    }
                    _ => {
                        return Err(Exception::IllegalInstruction(inst));
                    }
                }
            }
            _ => {
                error!("not implemented yet");
            }
        }

        Ok(self.pc + 4)
    }
}
