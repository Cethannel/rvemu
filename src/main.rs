mod cpu;
mod dram;
mod bus;
mod params;
mod exceptions;

use log::error;

use crate::cpu::*;

use std::{env, fs::File, io::{self, Read}};

fn main() -> io::Result<()> {
    pretty_env_logger::init();

    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        panic!("Usage: rvemu-simple <filename>");
    }
    let mut file = File::open(&args[1])?;
    let mut code = Vec::new();
    file.read_to_end(&mut code)?;

    let mut cpu = Cpu::new(code);

    loop {
        let inst = match cpu.fetch() {
            Ok(inst) => inst,
            Err(e) => {
                error!("{}", e);
                break;
            },
        };

        match cpu.execute(inst) {
            Ok(new_pc) => cpu.pc = new_pc,
            Err(e) => {
                error!("{}", e);
                break;
            },
        }

        if cpu.pc == 0 {
            break;
        }
    }
    cpu.dump_registers();

    Ok(())
}
