use std::fmt::Display;

pub enum Exception {
    LoadAccessFault(u64),
    StoreAMOAccessFault(u64),
    IllegalInstruction(u64),
}

impl Display for Exception {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Exception::LoadAccessFault(addr) => write!(f, "LoadAcessFault at addr: {}", addr),
            Exception::StoreAMOAccessFault(addr) => write!(f, "StoreAMOAccessFault at addr: {}", addr),
            Exception::IllegalInstruction(inst) => write!(f, "Illegal Instruction: {}", inst),
        }
    }
}
